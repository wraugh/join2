/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
'use strict'

const { Transform } = require('readable-stream')

const join2 = (options = {}) => {
  return new Transform(Object.assign({}, options, {
    transform (chunk, encoding, callback) {
      if (this.buf == null) {
        this.buf = chunk
      } else {
        this.push(this.buf + chunk)
        this.buf = null
      }
      callback()
    },
    flush (callback) {
      if (this.buf != null) {
        this.push(this.buf)
      }
      callback()
    }
  }))
}

module.exports = join2
