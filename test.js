/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
'use strict'

const { Transform, Writable } = require('readable-stream')
const tap = require('tap')

// #snip "example"
const split2 = require('split2')
const join2 = require('./index.js')

tap.test('Add line numbers to every line', t => {
  const addLineNos = new Transform({
    objectMode: true,
    transform: (chunk, encoding, callback) => {
      if (this.lineNo === undefined) this.lineNo = 0
      this.lineNo++
      callback(null, this.lineNo + ' ' + chunk)
    }
  })

  const inputChunks = [
    'so much',
    ' depends\nupon',
    '\n\n',
    'a red wheel\n',
    'bar',
    'row\n'
  ]
  const expectedOutputChunks = [
    '1 so much depends\n',
    '2 upon\n',
    '3 \n',
    '4 a red wheel\n',
    '5 barrow\n'
  ]
  t.plan(expectedOutputChunks.length)

  const dest = new Writable({
    objectMode: true,
    write: (chunk, encoding, callback) => {
      const expected = expectedOutputChunks.shift()
      t.equal(chunk, expected)
      callback()
    }
  })

  const src = split2(/(\n)/) /* Notice the capturing parens; see
                              * https://github.com/dominictarr/split#keep-matched-splitter */
  src.pipe(join2())
    .pipe(addLineNos)
    .pipe(dest)
  inputChunks.forEach(chunk => src.write(chunk))
  src.end()
})
// #snip

// #snip "usage"
// **join2(options)**
//
//  - `options`  *Object*  Options object passed to parent stream.Transform's
//                         constructor. **Default:** `{}`
//  - Returns: *stream.Transform*
//
// Creates a Transform stream that concatenates every two input chunks into a
// single output chunk. Returns said transform stream.

// If the input stream ends on an odd-numbered chunk, that last chunk is output
// as-is.
tap.test('Odd chunk count', t => {
  t.plan(1)
  const src = join2()
  const dest = new Writable({
    write (chunk, encoding, callback) {
      t.equal(chunk.toString(), 'foobar')
    }
  })
  src.pipe(dest)
  src.end('foobar')
})
// #snip
