#!/usr/bin/env node
/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
'use strict'

// This script writes README.md

const fs = require('fs')
const { code2doc } = require('marcco')
const { rewriteRequires } = require('public-requires')
const snippin = require('snippin')
const ST = require('stream-template')

// presentation helpers
const s = snippin()
const rr = src => rewriteRequires('join2', __dirname, __dirname, src)
const js2doc = src => code2doc(rr(src), { codePrefix: '~~~javascript' })
const pin = (file, snippet) => s.getSnippets(file).then(snippets => snippets[snippet])

process.chdir(__dirname)
const out = fs.createWriteStream('./README.md')

ST`Sometimes you want to work on a text stream one line at a time.
[split2](https://www.npmjs.com/package/split2) lets you do just that, but it's
got an annoying limitation: either it will

 1. strip out all newlines for you, in which case you need to add them back in
    yourself, or
 2. you can [keep the newlines](https://github.com/dominictarr/split#keep-matched-splitter),
    but they'll be in separate chunks all on their own, and you're no longer
    working line-by-line anymore.

The problem with the first approach is that you lose information. Did the
original stream end with a newline or not? Did it use Unix or Windows-style
newlines, or perhaps [some other line boundary supported by Unicode](
http://www.unicode.org/reports/tr18/#Line_Boundaries)? If you can't abide that,
you've got to use option #2.

This is where join2 comes in. It's a simple transform that combines every pair
of chunks into a single chunk. So you setup split2 with a newline-capturing
regex, then pipe it straight into join2, and out comes your original stream,
chunked line by line.

Example
-------

${pin('test.js', 'example').then(s => js2doc(s))}

Usage
-----

${pin('test.js', 'usage').then(s => js2doc(s))}

Contributing
------------

You're welcome to contribute to this project following the
[C4 process](https://rfc.zeromq.org/spec:42/C4/).

All patches must follow [standard style](https://standardjs.com/) and have 100%
test coverage. You can make sure of this by adding

    ./.pre-commit

to your git pre-commit hook.
`.pipe(out)
